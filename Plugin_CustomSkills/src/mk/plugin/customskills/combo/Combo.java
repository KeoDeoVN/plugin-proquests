package mk.plugin.customskills.combo;

import java.util.List;

import mk.plugin.customskills.config.Configs;

public class Combo {
	
	private List<Integer> comboIndexes;
	
	public Combo(List<Integer> comboIndexes) {
		this.comboIndexes = comboIndexes;
	}
	
	public List<Integer> getIndexes() {
		return this.comboIndexes;
	}
	
	public void add(int index) {
		this.comboIndexes.add(index);
	}
	
	public String getPermission() {
		for (Combo cb : Configs.COMBO_PERMISSIONS.keySet()) {
			if (cb.equals(this)) return Configs.COMBO_PERMISSIONS.get(cb);
		}
		return "ditme.concacdumamay";
	}
	
	public int getCooldown() {
		for (Combo cb : Configs.COMBO_DELAYS.keySet()) {
			if (cb.equals(this)) return Configs.COMBO_DELAYS.get(cb);
		}
		return 0;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Combo == false) return false;
		Combo combo = (Combo) o;
		return this.comboIndexes.equals(combo.getIndexes());
	}
	
}
