package mk.plugin.customskills.listener;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;

import mk.plugin.customskills.combo.ComboClick;
import mk.plugin.customskills.combo.ComboUtils;

public class ComboListener implements Listener {
	
	/*
	 * Add click to combo
	 */
	@EventHandler
	public void onClick(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		
		// Check if being in combo status
		if (!player.isSneaking()) return;
		
		// Check click
		Action action = e.getAction();
		ComboClick cc = ComboClick.fromAction(action);
		if (cc == null) return;
		
		// Get and add click
		ComboUtils.addClick(player, cc.getIndex());
		
		// Show combo
		ComboUtils.showCombo(player, ComboUtils.getCombo(player));
		player.playSound(player.getLocation(), Sound.BLOCK_WOOD_BUTTON_CLICK_ON, 1, 1);
	}
	
	/*
	 * Check remove combo status
	 */
	@EventHandler
	public void onToggleSneak(PlayerToggleSneakEvent e) {
		Player player = e.getPlayer();
		if (player.isSneaking()) ComboUtils.remove(player);
	}
	
}
