package mk.plugin.customskills.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.Faction;

import mk.plugin.customskills.main.MainCustomSkills;

public class Utils {
	
	private static Map<Integer, Long> cooldownDamage = new HashMap<Integer, Long> ();
	
	public static void runCommand(Player player, String cmd) {
		// Check player
		if (cmd.startsWith("{player}")) {
			cmd = cmd.replace("{player} ", "");
			Bukkit.dispatchCommand(player, cmd.replace("%player%", player.getName()));
			return;
		}
		
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd.replace("%player%", player.getName()));
	}
	
	public static boolean rate(double rate) {
		if (rate >= 100) return true;
		rate = rate * 100;
		int random = new Random().nextInt(10000);
		if (random < rate) {
			return true;
		} else return false;
	}
	
	public static double random(double min, double max) {
		return (new Random().nextInt(new Double((max - min) * 1000).intValue()) + min * 1000) / 1000;
	}
	
	public static boolean isCooldownDamage(int entityID) {
		if (cooldownDamage.containsKey(entityID)) {
			if (cooldownDamage.get(entityID) > System.currentTimeMillis()) return true;
			return false;
		}
		return false;
	}
	
	public static void setCooldownDamage(int entityID, long timeMilis) {
		cooldownDamage.put(entityID, System.currentTimeMillis() + timeMilis);
	}
	
	public static void removeCooldownDamage(int entityID) {
		cooldownDamage.remove(entityID);
	}
	
	public static void damage(Player player, LivingEntity target, double damage, long cooldown) {
		if (target.isDead()) return;
		if (isCooldownDamage(target.getEntityId())) return;
		setCooldownDamage(target.getEntityId(), cooldown);		
		target.damage(damage, player);
	}
	
	public static void damageEntitiesNearby(Player player, Location location, double x, double y, double z, double damage, long damageCooldown) {
		location.getWorld().getNearbyEntities(location, x, y, z).forEach(e -> {
			if (e != player && e instanceof LivingEntity) {
				LivingEntity le = (LivingEntity) e;
				damage(player, le, damage, damageCooldown);
			}
		});
	}
	
	public static List<LivingEntity> getLivingEntities(Player player, Location location, double x, double y, double z) {
		List<LivingEntity> list = Lists.newArrayList();
		location.getWorld().getNearbyEntities(location, x, y, z).forEach(e -> {
			if (e != player && e instanceof LivingEntity) {
				LivingEntity le = (LivingEntity) e;
				list.add(le);
			}
		});
		return list;
	}
	
	public static void collisionDamage(Player player, double damage, long damageCooldown, long interval) {
		long start = System.currentTimeMillis();
		new BukkitRunnable() {
			@Override
			public void run() {
				if (System.currentTimeMillis() - start >= interval) {
					this.cancel();
					return;
				}
				damageEntitiesNearby(player, player.getLocation(), 1, 1, 1, damage, damageCooldown);
			}
		}.runTaskTimer(MainCustomSkills.get(), 0, 1);
	}
	
	public static boolean canCastSkillHere(Player player) {
		// Check safezone
		if (Bukkit.getPluginManager().isPluginEnabled("Factions")) {
			FLocation fl = new FLocation(player.getLocation());
			Faction f = Board.getInstance().getFactionAt(fl);
			if (f.isSafeZone()) return false;
		}
		
		//...
		
		return true;
	}
	
	public static LivingEntity getTarget(Player source, double range) {
		List<Block> blocksInSight = source.getLineOfSight(Sets.newHashSet(Material.AIR), new Double(range).intValue());
		List<Entity> nearEntities = source.getNearbyEntities(range, range, range);
		
		if (blocksInSight != null && nearEntities != null) {
			for (Block block : blocksInSight) {
				int xBlock = block.getX();
				int yBlock = block.getY();
				int zBlock = block.getZ();

				for (Entity entity : nearEntities) {
					if (!(entity instanceof LivingEntity)) continue;
					Location entityLocation = entity.getLocation();
					int xEntity = entityLocation.getBlockX();
					int yEntity = entityLocation.getBlockY();
					int zEntity = entityLocation.getBlockZ();
					
					// Check near
					if (xEntity == xBlock && (Math.abs(yBlock - yEntity) < 2) && zEntity == zBlock) {
						return (LivingEntity) entity;
					}
					
				}
			}
		}
		return null;
	}
	
	public static List<Location> circle(Location location, double radius) {
		int amount = new Double(radius * 20).intValue();
		double increment = (2 * Math.PI) / amount;
        ArrayList<Location> locations = new ArrayList<Location>();
        
        for (int i = 0 ; i < amount ; i++) {
            double angle = i * increment;
            double x = location.getX() + (radius * Math.cos(angle));
            double z = location.getZ() + (radius * Math.sin(angle));
            locations.add(new Location(location.getWorld(), x, location.getY(), z));
        }
        
        return locations;
	}
	
	public static void circleParticles(Particle particle, Location location, double radius) {
		int amount = new Double(radius * 20).intValue();
		double increment = (2 * Math.PI) / amount;
        ArrayList<Location> locations = new ArrayList<Location>();
        
        for (int i = 0 ; i < amount ; i++) {
            double angle = i * increment;
            double x = location.getX() + (radius * Math.cos(angle));
            double z = location.getZ() + (radius * Math.sin(angle));
            locations.add(new Location(location.getWorld(), x, location.getY(), z));
        }
        
        for (Location l : locations) {
//        	ParticleAPI.sendParticle(e, l, 0, 0, 0, 0, 1);
        	location.getWorld().spawnParticle(particle, l, 1, 0, 0, 0, 0);
        }
	}
	
}
