package mk.plugin.customskills.skill.executor;

import java.util.Map;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import mk.plugin.customskills.main.MainCustomSkills;
import mk.plugin.customskills.player.PlayerTimings;
import mk.plugin.customskills.skill.SkillExecutor;
import mk.plugin.customskills.util.Utils;

public class IttouShuraExecutor implements SkillExecutor {

	@Override
	public void run(Player player, Map<String, Object> map) {
		int potionLevel = ((Double) map.get("effect-level")).intValue();
		int effectTime = new Double(((Double) map.get("effect-time")) * 20).intValue();
		double effectRadius = (Double) map.get("effect-radius");
		
		// Effect
		Utils.getLivingEntities(player, player.getLocation(), effectRadius, effectRadius, effectRadius).forEach(le -> {
			le.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, effectTime, potionLevel));
		});
		
		// Add passive
		PlayerTimings.add(player, "ittouShuraPassive", 4000);
		
		// Particles
		new BukkitRunnable() {
			@Override
			public void run() {
				if (!PlayerTimings.has(player, "ittouShuraPassive")) {
					this.cancel();
					return;
				}
				player.getWorld().spawnParticle(Particle.SMOKE_LARGE, player.getLocation().add(0, 0.7, 0), 10, 0.5, 0.5, 0.5, 0);
			}
		}.runTaskTimerAsynchronously(MainCustomSkills.get(), 0, 4);
	}
	
	public static void execute(EntityDamageByEntityEvent e, Player player) {
		if (PlayerTimings.has(player, "ittouShuraPassive")) {
			// Random
			double x = player.getLocation().getX() + Utils.random(-5, 5);
			double z = player.getLocation().getZ() + Utils.random(-5, 5);
			Location temp = new Location(player.getLocation().getWorld(), x, player.getLocation().getY(), z);
			
			// Get safe
			int j = 0;
			while (temp.getBlock().getType() != Material.AIR) {
				j++;
				if (j > 10) {
					return;
				}
				temp = temp.add(0, 1, 0);
			}
			
			// Tele
			player.teleport(temp);
		}
	}
	
}
