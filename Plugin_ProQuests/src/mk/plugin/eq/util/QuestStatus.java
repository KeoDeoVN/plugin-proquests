package mk.plugin.eq.util;

import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import me.blackvein.quests.CustomObjective;
import me.blackvein.quests.Quest;
import me.blackvein.quests.Quester;
import me.blackvein.quests.Quests;
import me.clip.placeholderapi.PlaceholderAPI;
import mk.plugin.playerdata.storage.PlayerData;
import mk.plugin.playerdata.storage.PlayerDataAPI;
import mk.plugin.proquests.help.QHelp;

public enum QuestStatus {
	
	CANT_DO("Chưa thể làm", Material.BOOK, "§c") {
		@Override
		public ItemStack getIcon(Player player, Quest quest) {
			ItemStack item = new ItemStack(this.getMaterial());
			ItemStackUtils.setDisplayName(item, "§6§l" + quest.getName());
			List<String> lore = Lists.newArrayList();
//			lore.add(this.getColor() + this.getName());
//			lore.add("");
//			lore.add("§aYêu cầu: ");
			QuestUtils.getRequirements(quest).forEach(s -> {
				lore.add("§c§o" + s.replace("&", "§"));
			});
			ItemStackUtils.setLore(item, lore);
			ItemStackUtils.addFlag(item, ItemFlag.HIDE_ATTRIBUTES);
			
			return item;
		}
	},
	CAN_DO("Có thể làm", Material.ENCHANTED_BOOK, "§a") {
		@Override
		public ItemStack getIcon(Player player, Quest quest) {
			ItemStack item = new ItemStack(this.getMaterial());
			ItemStackUtils.setDisplayName(item, "§6§l" + quest.getName());
			
			List<String> lore = Lists.newArrayList();
			String desc = QuestUtils.getDesc(quest);
//			lore.add(this.getColor() + this.getName());
			lore.addAll(Utils.toList(desc, 25, "§7§o"));
			lore.add("");
			lore.add("§aCó thể làm, phần thưởng:");
			List<String> rewards = QuestUtils.getRewards(quest);
			rewards.forEach(s -> {
				lore.add(" §f" + s.replace("&", "§"));
			});
			ItemStackUtils.setLore(item, lore);
			ItemStackUtils.addFlag(item, ItemFlag.HIDE_ATTRIBUTES);
			
			return item;
		}
	},
	STARTED("Đang làm", Material.BOOK_AND_QUILL, "§e") {
		@SuppressWarnings("deprecation")
		@Override
		public ItemStack getIcon(Player player, Quest quest) {
			ItemStack item = new ItemStack(Material.BOOK_AND_QUILL);
			Quester qt = QuestUtils.getQuestsPlugin().getQuester(player.getUniqueId());
			ItemStackUtils.setDisplayName(item, "§6§l" + quest.getName());
			
			List<String> lore = Lists.newArrayList();
			lore.add("§aTiến trình: §f" + QuestUtils.getStageInfo(quest, qt.getCurrentQuests().get(quest)));
			lore.add("§aYêu cầu:");
			
			Quests quests = (Quests) Bukkit.getPluginManager().getPlugin("Quests");
			Quester quester = quests.getQuester(player.getUniqueId());
			if (quester.getCurrentStage(quest) != null) {
				for (CustomObjective o : quester.getCurrentStage(quest).getCustomObjectives()) {
					if (o.getName().equalsIgnoreCase("Placeholder Change")) {
						double v = getReal(player, o, quest);
						double r = getRequirement(player, o, quest);
	
						LinkedList<Entry<String, Object>> l = quester.getCurrentStage(quest).getCustomObjectiveData();
						for (Entry<String, Object> e : l) {
							if (e.getKey().equalsIgnoreCase("Add")) {
								double add = Double.valueOf(e.getValue().toString());
								double current = (add - (r - v));
								lore.add("§e§o [" + current + "/" + add + "]");
								break;
							}
						}
					}
				}
			}
			
			qt.getObjectives(quest, false).forEach(s -> lore.addAll(Utils.toList(ChatColor.stripColor(s), 25, "§e§o ")));
			if (QHelp.hasHelp(player, quest)) {
				lore.add("§aGợi ý:");
				lore.addAll(Utils.toList(QHelp.getHelp(player, quest), 25, "§7§o "));
			}
			lore.add("");
			lore.add("§cShift + Click để hủy Quest");
			ItemStackUtils.setLore(item, lore);
			
			return item;
		}
	},
	FINISHED("Đã hoàn thành", Material.KNOWLEDGE_BOOK, "§6") {
		@Override
		public ItemStack getIcon(Player player, Quest quest) {
			ItemStack item = new ItemStack(this.getMaterial());
			ItemStackUtils.setDisplayName(item, "§6§l" + quest.getName());
			ItemStackUtils.addLoreLine(item, this.getColor() + this.getName());
			ItemStackUtils.addFlag(item, ItemFlag.HIDE_ATTRIBUTES);
			return item;
		}
	};
	
	private String name;
	private Material material;
	private String color;
	
	public abstract ItemStack getIcon(Player player, Quest quest);
	
	private QuestStatus(String name, Material material, String color) {
		this.name = name;
		this.material = material;
		this.color = color;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Material getMaterial() {
		return this.material;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public static final String KEY = "qs-qc-%placeholder%";
	
	private static double getRequirement(Player p, CustomObjective o, Quest quest) {
		PlayerData pd = PlayerDataAPI.get(p, "quests");
		for (Entry<String, String> e : Maps.newHashMap(pd.getDataMap()).entrySet()) {
			String k = e.getKey();
			String v = e.getValue();
			if (k.contains(KEY.replace("%placeholder%", ""))) {
				String plh = k.replace(KEY.replace("%placeholder%", ""), "");
				String qplh = o.getDataForPlayer(p, o, quest).get("Placeholder").toString();
				if (!plh.equalsIgnoreCase(qplh)) continue;
				double tv = Double.valueOf(v);
				return tv;
			}
		}
		return -1;
	}
	
	private static double getReal(Player p, CustomObjective o, Quest quest) {
		PlayerData pd = PlayerDataAPI.get(p, "quests");
		for (Entry<String, String> e : Maps.newHashMap(pd.getDataMap()).entrySet()) {
			String k = e.getKey();
			if (k.contains(KEY.replace("%placeholder%", ""))) {
				String plh = k.replace(KEY.replace("%placeholder%", ""), "");
				String qplh = o.getDataForPlayer(p, o, quest).get("Placeholder").toString();
				if (!plh.equalsIgnoreCase(qplh)) continue;
				double realv = Double.valueOf(PlaceholderAPI.setPlaceholders(p, plh));
				return realv;
			}
		}
		return -1;
	}
	
}
