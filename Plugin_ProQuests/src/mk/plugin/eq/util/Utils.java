package mk.plugin.eq.util;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffectType;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class Utils {
	
	public static double random(double min, double max) {
		return (new Random().nextInt(new Double((max - min) * 1000).intValue()) + min * 1000) / 1000;
	}
	
	public static int randomInt(int min, int max) {
		if (min == max) return min;
		return new Random().nextInt(max - min + 1) + min;
	}
	
	public static String getRomanNumber(int lv) {
		Map<Integer, String> map = new HashMap<Integer, String> ();
		map.put(1, "I");
		map.put(2, "II");
		map.put(3, "III");
		map.put(4, "IV");
		map.put(5, "V");
		map.put(6, "VI");
		map.put(7, "VII");
		map.put(8, "VIII");
		map.put(9, "IX");
		map.put(10, "X");
		map.put(11, "XI");
		map.put(12, "XII");
		map.put(13, "XIII");
		map.put(14, "XIV");
		map.put(15, "XV");
		map.put(16, "XVI");
		map.put(17, "XVII");
		map.put(18, "XVIII");
		map.put(19, "XIX");
		map.put(20, "XX");
		
		if (map.containsKey(lv)) return map.get(lv);
		return "" + lv;
	}
	
	public static String getVNPotionName(PotionEffectType type) {
		Map<PotionEffectType, String> map = Maps.newHashMap();
		map.put(PotionEffectType.ABSORPTION, "Hấp thụ");
		map.put(PotionEffectType.BLINDNESS, "Gây mù");
//		map.put(PotionEffectType.CONDUIT_POWER, "Sức mạnh thủy triều");
		map.put(PotionEffectType.CONFUSION, "Nhầm lẫn");
		map.put(PotionEffectType.DAMAGE_RESISTANCE, "Kháng cự");
//		map.put(PotionEffectType.DOLPHINS_GRACE, "Cá heo");
		map.put(PotionEffectType.FAST_DIGGING, "Đào nhanh");
		map.put(PotionEffectType.FIRE_RESISTANCE, "Kháng lửa");
		map.put(PotionEffectType.GLOWING, "Phát sáng");
		map.put(PotionEffectType.HARM, "Hại");
		map.put(PotionEffectType.HEAL, "Hồi phục");
		map.put(PotionEffectType.HEALTH_BOOST, "Tăng máu");
		map.put(PotionEffectType.HUNGER, "Gây đói");
		map.put(PotionEffectType.INCREASE_DAMAGE, "Tăng sát thương");
		map.put(PotionEffectType.INVISIBILITY, "Vô hình");
		map.put(PotionEffectType.JUMP, "Nhảy cao");
		map.put(PotionEffectType.LEVITATION, "Lơ lửng");
		map.put(PotionEffectType.LUCK, "May mắn");
		map.put(PotionEffectType.NIGHT_VISION, "Nhìn đêm");
		map.put(PotionEffectType.POISON, "Gây độc");
		map.put(PotionEffectType.REGENERATION, "Hồi phục");
		map.put(PotionEffectType.SATURATION, "Thấm vào");
		map.put(PotionEffectType.SLOW, "Gây chậm");
		map.put(PotionEffectType.SLOW_DIGGING, "Đào chậm");
//		map.put(PotionEffectType.SLOW_FALLING, "Rơi chậm");
		map.put(PotionEffectType.SPEED, "Tốc độ");
		map.put(PotionEffectType.UNLUCK, "Đen đủi");
		map.put(PotionEffectType.WATER_BREATHING, "Thở dưới nước");
		map.put(PotionEffectType.WEAKNESS, "Yếu đuối");
		map.put(PotionEffectType.WITHER, "Khô héo");
		
		return map.get(type);
	}
	
	public static void giveItem(Player player, ItemStack item) {
		PlayerInventory inv = player.getInventory();
		boolean full = inv.firstEmpty() == -1;
		if (!full) {
			inv.addItem(item);
		} 	
	}
	
	public static void broadcast(String mess) {
		Bukkit.getOnlinePlayers().forEach(p -> {
			p.sendMessage(mess);
		});
		Bukkit.getConsoleSender().sendMessage(mess);
	}
	
	public static List<LivingEntity> getLivingEntities(Player player, Location location, double x, double y, double z) {
		List<LivingEntity> list = Lists.newArrayList();
		location.getWorld().getNearbyEntities(location, 5, 5, 5).stream().filter(e -> e instanceof LivingEntity && e != player).collect(Collectors.toList()).forEach(e -> {
			list.add((LivingEntity) e);
		});;
		return list;
	}
	
	public static void circleParticles(Particle particle, Location location, double radius) {
		int amount = new Double(radius * 20).intValue();
		double increment = (2 * Math.PI) / amount;
        ArrayList<Location> locations = new ArrayList<Location>();
        
        for (int i = 0 ; i < amount ; i++) {
            double angle = i * increment;
            double x = location.getX() + (radius * Math.cos(angle));
            double z = location.getZ() + (radius * Math.sin(angle));
            locations.add(new Location(location.getWorld(), x, location.getY(), z));
        }
        
        for (Location l : locations) {
//        	ParticleAPI.sendParticle(e, l, 0, 0, 0, 0, 1);
        	location.getWorld().spawnParticle(particle, l, 1, 0, 0, 0, 0);
        }
	}
	
	public static double round(double i) {
		DecimalFormat df = new DecimalFormat("#.##"); 
		String s = df.format(i).replace(",", ".");
		double newDouble = Double.valueOf(s);
		
		return newDouble;
	}
	
	public static boolean rate(double tiLe) {
		if (tiLe >= 100) return true;
		double rate = tiLe * 100;
		int random = new Random().nextInt(10000);
		if (random < rate) {
			return true;
		} else return false;
	}
	
	public static void addHealth(Player player, double value) {
		double maxHealth = player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
		player.setHealth(Math.min(player.getHealth() + value, maxHealth));
	}
	
	public static ItemStack getBlackSlot() {
		ItemStack other = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
		ItemMeta meta = other.getItemMeta();
		meta.setDisplayName(" ");
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		other.setItemMeta(meta);
		return other;
	}
	
	public static List<ItemStack> getItemsEquiped(Player player) {
		List<ItemStack> list = Lists.newArrayList();
		
		// Armor
		for (ItemStack item : player.getInventory().getArmorContents()) {
			if (item != null && item.getType() != Material.AIR) list.add(item);
		}
		
		// Hand
		ItemStack itemHand = player.getInventory().getItemInMainHand();
		if (itemHand != null && itemHand.getType() != Material.AIR) list.add(itemHand);
		
		// Offhand
		ItemStack offHand = player.getInventory().getItemInOffHand();
		if (offHand != null) {
			list.add(offHand);
		}
		
		return list;
	}
	
	public static List<String> toList(String s, int length, String start) {
		List<String> result = new ArrayList<String>();
		if (s == null)
			return result;
		if (!s.contains(" ")) {
			result.add(s);
			return result;
		}

		String[] words = s.split(" ");
		int l = 0;
		String line = "";
		for (int i = 0; i < words.length; i++) {
			l += words[i].length();
			if (l > length) {
				result.add(line.substring(0, line.length() - 1));
				l = words[i].length();
				line = "";
				line += words[i] + " ";
			} else {
				line += words[i] + " ";
			}
		}

		if (!line.equalsIgnoreCase(" "))
			result.add(line);

		for (int i = 0; i < result.size(); i++) {
			result.set(i, start + result.get(i));
		}

		return result;
	}
	
}
