package mk.plugin.eq.main;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Lists;

import mk.plugin.eq.command.Commands;
import mk.plugin.eq.listener.EventListener;
import mk.plugin.eq.object.DQCategory;
import mk.plugin.eq.object.DQRequirement;
import mk.plugin.eq.util.DQUtils;
import mk.plugin.proquests.gui.CustomGUI;
import mk.plugin.proquests.gui.GUIData;
import mk.plugin.proquests.gui.SpecialGUI;
import mk.plugin.proquests.help.QHelp;

public class MainProQuests extends JavaPlugin {

	public static MainProQuests plugin;
	public static int GUI_SIZE = 0;
	
	private FileConfiguration config;
	
	@Override
	public void onEnable() {
		plugin = this;
		this.saveDefaultConfig();
		this.reloadConfig();
		
		
		// Command
		this.getCommand("eq").setExecutor(new Commands());
		
		// Listener
		Bukkit.getPluginManager().registerEvents(new EventListener(), this);
		
		// 
//		Bukkit.getOnlinePlayers().forEach(player -> DQUtils.checkData(player));
		
		// Task check
//		Bukkit.getScheduler().runTaskTimerAsynchronously(this, () -> {
//			LocalTime time = LocalTime.of(1, 0);
//			int s = LocalTime.now().getSecond() - time.getSecond();
//			int m = LocalTime.now().getMinute() - time.getMinute();
//			int h = LocalTime.now().getHour() - time.getHour();
//			if (h == 0 && m == 0 && s < 1) {
//				Bukkit.getOnlinePlayers().forEach(player -> DQUtils.checkData(player));
//			}
//		}, 0, 20);
		
		Bukkit.getScheduler().runTaskTimerAsynchronously(this, () -> {
			Bukkit.getOnlinePlayers().forEach(player -> DQUtils.checkData(player));
		}, 0, 20);
	}

	public FileConfiguration getConfig() {
		return this.config;
	}
	
	public void reloadConfig() {
		File file = new File(this.getDataFolder(), "config.yml");
		config = YamlConfiguration.loadConfiguration(file);
		
		// Daily Quests
		DQUtils.DQCategories.clear();
		config.getConfigurationSection("dailyquest.category").getKeys(false).forEach(id -> {
			List<String> quests = Lists.newArrayList();
			config.getStringList("dailyquest.category." + id + ".quests").forEach(qn -> {
				quests.add(qn);
			});;
			
			int min = config.getInt("dailyquest.category." + id + ".min-quests");
			int max = config.getInt("dailyquest.category." + id + ".max-quests");
	
			int minLv = 0;
			if (config.contains("dailyquest.category." + id + ".requirement.min-level")) minLv = config.getInt("dailyquest.category." + id + ".requirement.min-level");
			int qP = 0;
			if (config.contains("dailyquest.category." + id + ".requirement.quest-point")) qP = config.getInt("dailyquest.category." + id + ".requirement.quest-point");
			List<String> qD = Lists.newArrayList();
			if (config.contains("dailyquest.category." + id + ".requirement.quest-done")) qD = config.getStringList("dailyquest.category." + id + ".requirement.quest-done");
			List<String> data = Lists.newArrayList();
			if (config.contains("dailyquest.category." + id + ".requirement.playerdata")) data = config.getStringList("dailyquest.category." + id + ".requirement.playerdata");
			
			DQRequirement r = new DQRequirement(minLv, qP, qD, data);
			
			DQUtils.DQCategories.add(new DQCategory(quests, min, max, r));
		});
		
		// Special 
		config.getConfigurationSection("special-gui").getKeys(false).forEach(id -> {
			String title = config.getString("special-gui." + id + ".title").replace("&", "§");
			int slot = config.getInt("special-gui." + id + ".slot");
			String name = config.getString("special-gui." + id + ".name").replace("&", "§");
			Material icon = Material.valueOf(config.getString("special-gui." + id + ".icon"));
			String desc = config.getString("special-gui." + id + ".desc").replace("&", "§");
			List<String> cmds = config.getStringList("special-gui." + id + ".commands");
			GUIData dg = new GUIData(title, slot, name, icon, desc, cmds);
			SpecialGUI.valueOf(id.toUpperCase()).setGUIData(dg);
		});
		
		// Custom GUI
		CustomGUI.guis.clear();
		config.getConfigurationSection("custom-gui").getKeys(false).forEach(id -> {
			String title = config.getString("custom-gui." + id + ".title").replace("&", "§");
			int slot = config.getInt("custom-gui." + id + ".slot");
			String name = config.getString("custom-gui." + id + ".name").replace("&", "§");
			Material icon = Material.valueOf(config.getString("custom-gui." + id + ".icon"));
			String desc = config.getString("custom-gui." + id + ".desc").replace("&", "§");
			List<String> cmds = config.getStringList("custom-gui." + id + ".commands");
			List<String> quests = config.getStringList("custom-gui." + id + ".quests");
			GUIData gd = new GUIData(title, slot, name, icon, desc, cmds);
			CustomGUI.guis.put(id, new CustomGUI(gd, quests));
		});
		
		// Quests' help
		QHelp.reload(config);
		
		// Gui's size
		if (config.contains("size")) GUI_SIZE = config.getInt("size");
		
	}
	
	public void saveConfig() {
		File file = new File(this.getDataFolder(), "config.yml");
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
