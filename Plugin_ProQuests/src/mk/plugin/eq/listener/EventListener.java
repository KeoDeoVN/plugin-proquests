package mk.plugin.eq.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import me.blackvein.quests.Quest;
import me.blackvein.quests.Quester;
import me.blackvein.quests.events.quester.QuesterPostChangeStageEvent;
import me.blackvein.quests.events.quester.QuesterPostStartQuestEvent;
import me.blackvein.quests.events.quester.QuesterPreCompleteQuestEvent;
import mk.plugin.eq.main.MainProQuests;
import mk.plugin.eq.util.DQData;
import mk.plugin.eq.util.DQUtils;
import mk.plugin.eq.util.QuestUtils;
import mk.plugin.proquests.gui.CustomGUI;
import mk.plugin.proquests.gui.GUIOpener;
import mk.plugin.proquests.gui.SpecialGUI;
import mk.plugin.proquests.utils.StageUtils;

public class EventListener implements Listener {
	
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		GUIOpener.eventClick(e);
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		Bukkit.getScheduler().runTaskLaterAsynchronously(MainProQuests.plugin, () -> {
			DQUtils.loadData(player);
			if (DQUtils.checkData(player)) {
				Bukkit.getScheduler().runTaskLaterAsynchronously(MainProQuests.plugin, () -> {
//				player.sendMessage("§aNhiệm vụ hằng ngày được khởi tạo lại!	");
				}, 5);
			}
		}, 20);
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Bukkit.getScheduler().runTaskLaterAsynchronously(MainProQuests.plugin, () -> {
			DQUtils.clearData(e.getPlayer());;
		}, 20);
	}

	@EventHandler 
	public void onFinishQuest(QuesterPreCompleteQuestEvent e) {
		Quester quester = e.getQuester();
		String quest = e.getQuest().getName();
		if (QuestUtils.isDailyQuest(quest)) {
			Player player = Bukkit.getPlayer(quester.getUUID());
			DQData data = DQUtils.getData(player);
			data.addDoneQuests(quest);
			DQUtils.saveData(player);
		}
	}
	
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e) {
		String cmd = e.getMessage().replace("/", "");
		Player player = e.getPlayer();
		if (cmd.equalsIgnoreCase("quest") || cmd.equalsIgnoreCase("nhiemvu")) {
			e.setCancelled(true);
			GUIOpener.openMainGUI(player);
			return;
		}
		
		for (SpecialGUI sg : SpecialGUI.values()) {
			if (sg.getGUIData().getCommands().contains(cmd.toLowerCase())) {
				e.setCancelled(true);
				GUIOpener.openGUI(player, sg.getGUIData(), sg.getQuests(player), 1);
				return;
			}
		}
		
		for (CustomGUI cg : CustomGUI.guis.values()) {
			if (cg.getGUIData().getCommands().contains(cmd.toLowerCase())) {
				e.setCancelled(true);
				GUIOpener.openGUI(player, cg.getGUIData(), cg.getQuests(), 1);
				return;
			}
		}
		
	}
	
	@EventHandler
	public void onStageComplete(QuesterPostChangeStageEvent e) {
		if (e.getNextStage() != null) {
			Player player = e.getQuester().getPlayer();
			Quest quest = e.getQuest();
			StageUtils.showObjective(player, quest);
		}
	}
	
	@EventHandler
	public void onStartQuest(QuesterPostStartQuestEvent e) {
		Player player = e.getQuester().getPlayer();
		Quest quest = e.getQuest();
		StageUtils.showObjective(player, quest);
	}
	
}
